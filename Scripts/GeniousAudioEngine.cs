﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeniousAudioEngine : AGeniousAudioEngine
{
    readonly int m_HashFootFall = Animator.StringToHash("FootFall");

    protected override void FixedUpdate()
    {
        PlayAudio();
    }

    //public RandomAudioPlayer footstepPlayer;         // Random Audio Players used for various situations.
    /*
        Material m_CurrentWalkingSurface 
    {
        get 
        {
            return fc?.locomotion?.m_CurrentWalkingSurface;
        }
    }
     */
    void PlayAudio() //Handle footsteps and things here... Probably dependent on State
    {
       /*  float footfallCurve = fc.m_Animator.GetFloat(m_HashFootFall);

        if (footfallCurve > 0.01f && !footstepPlayer.playing && footstepPlayer.canPlay)
        {
            footstepPlayer.playing = true;
            footstepPlayer.canPlay = false;
            footstepPlayer.PlayRandomClip(m_CurrentWalkingSurface, m_ForwardSpeed < 4 ? 0 : 1);
        }
        else if (footstepPlayer.playing)
        {
            footstepPlayer.playing = false;
        }
        else if (footfallCurve < 0.01f && !footstepPlayer.canPlay)
        {
            footstepPlayer.canPlay = true;
        }

        if (fc.m_IsGrounded && !m_PreviouslyGrounded)
        {
            landingPlayer.PlayRandomClip(m_CurrentWalkingSurface, bankId: m_ForwardSpeed < 4 ? 0 : 1);
            emoteLandingPlayer.PlayRandomClip();
        }

        if (!m_IsGrounded && m_PreviouslyGrounded && m_VerticalSpeed > 0f)
        {
            emoteJumpPlayer.PlayRandomClip();
        }

        if (m_CurrentStateInfo.shortNameHash == m_HashHurt && m_PreviousCurrentStateInfo.shortNameHash != m_HashHurt)
        {
            hurtAudioPlayer.PlayRandomClip();
        }

        if (m_CurrentStateInfo.shortNameHash == m_HashEllenDeath && m_PreviousCurrentStateInfo.shortNameHash != m_HashEllenDeath)
        {
            emoteDeathPlayer.PlayRandomClip();
        }

        if (m_CurrentStateInfo.shortNameHash == m_HashEllenCombo1 && m_PreviousCurrentStateInfo.shortNameHash != m_HashEllenCombo1 ||
            m_CurrentStateInfo.shortNameHash == m_HashEllenCombo2 && m_PreviousCurrentStateInfo.shortNameHash != m_HashEllenCombo2 ||
            m_CurrentStateInfo.shortNameHash == m_HashEllenCombo3 && m_PreviousCurrentStateInfo.shortNameHash != m_HashEllenCombo3 ||
            m_CurrentStateInfo.shortNameHash == m_HashEllenCombo4 && m_PreviousCurrentStateInfo.shortNameHash != m_HashEllenCombo4)
        {
            emoteAttackPlayer.PlayRandomClip();
        }
        */
    }
    
}
